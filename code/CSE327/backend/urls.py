from django.urls import path, include
# from django.contrib.auth.views import LoginView
from .views import user_login, user_logout, user_register, dashboard, profile
from .views import FileUploadView

app_name = 'backend'

urlpatterns = [

    path('login/', user_login, name='login'),
    path('logout/', user_logout, name='logout'),
    path('register/', user_register, name='register'),

    path('user/<str:username>', profile, name='profile'),

    path('', dashboard, name='dashboard'),

    path('upload/', FileUploadView.as_view(), name='fileupload'),
]
