package com.nsu.classinsight.ui.auth.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.nsu.classinsight.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
}
