package com.nsu.classinsight.di.builder;


import com.nsu.classinsight.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {FragmentProvider.class})
    abstract MainActivity bindAuthActivity();

}
