package com.nsu.classinsight.di.module;


import android.app.Application;
import android.content.Context;

import androidx.room.Room;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nsu.classinsight.BuildConfig;
import com.nsu.classinsight.data.local.db.AppDatabase;
import com.nsu.classinsight.data.local.db.AppDbHelper;
import com.nsu.classinsight.data.local.db.DbHelper;
import com.nsu.classinsight.data.local.prefs.AppPreferencesHelper;
import com.nsu.classinsight.data.local.prefs.PreferencesHelper;
import com.nsu.classinsight.data.remote.ApiConstants;
import com.nsu.classinsight.data.remote.ApiService;
import com.nsu.classinsight.data.remote.CoreApiHandler;
import com.nsu.classinsight.data.remote.RequestInterceptor;
import com.nsu.classinsight.di.ApiInfo;
import com.nsu.classinsight.di.DatabaseInfo;
import com.nsu.classinsight.di.PreferenceInfo;
import com.nsu.classinsight.utils.AppConstants;
import com.nsu.classinsight.utils.rx.AppSchedulerProvider;
import com.nsu.classinsight.utils.rx.SchedulerProvider;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    @Provides
    @ApiInfo
    String provideApiKey(PreferencesHelper preferencesHelper) {
        return preferencesHelper.getAccessToken();
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(@DatabaseInfo String dbName, Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, dbName).fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(AppPreferencesHelper preferencesHelper) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.connectTimeout(ApiConstants.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.readTimeout(ApiConstants.READ_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.writeTimeout(ApiConstants.WRITE_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.addInterceptor(new RequestInterceptor(preferencesHelper));
        okHttpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        return okHttpClient.build();
    }

    @Provides
    @Singleton
    ApiService provideRetrofit(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    CoreApiHandler provideCoreApiHandler() {
        return new CoreApiHandler();
    }


    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

}
