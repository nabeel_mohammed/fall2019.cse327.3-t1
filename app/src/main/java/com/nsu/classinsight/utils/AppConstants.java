
package com.nsu.classinsight.utils;


public final class AppConstants {


    public static final String DB_NAME = "ekhata.db";
    public static final String PREF_NAME = "ekhata_pref";
    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "bearer "; // required space at word end
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    public static final String KEY_SHOP = "shop";
    public static final String KEY_SHOP_NAME = "shop_name";
    public static final String USER_TYPE_SHOP_OWNER = "ShopOwner";
    public static final String USER_TYPE_GENERAL = "General";
    public static  final String KEY_LAST_NAME = "last_name";
    public static  final String KEY_FIRST_NAME = "first_name";
    public static  final String KEY_USER_NAME = "username";
    public static  final String KEY_EMAIL = "email";
    public static  final String KEY_GROUPS = "groups";
    public static final String KEY_TRANSACTION = "transaction";
    public static  final String KEY_PRODUCT = "product";
    public static  final String KEY_CART_LIST = "cart_list";

    /**
     * TODO -> Remove this after image dynic work done.
     */
    public static  final String PRODUCT_IMAGE_URL = "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/iphone11-red-select-2019?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1566956144763";
    public static final String KEY_CATEGORY_SLUG = "category_slug";

    public static final String KEY_LEDGER_TYPE_DEBIT = "debit";
    public static final String KEY_LEDGER_TYPE_CREDIT = "credit";
    public static final String KEY_INVOICE_RESPONSE = "invoice_response";
    public static final String PAYMENT_METHOD_BKASH = "bkash";
    public static final String PAYMENT_METHOD_EVALY_PAY = "evaly_pay";
    public static final String PAYMENT_METHOD_GATEWAY = "payment_gateway";
    public static final String PAYMENT_METHOD_PAY_LATER = "pay_later";
    public static final int DRAWER_ITEM_HOME = 1001;
    public static final int DRAWER_ITEM_INVITE = 1002;
    public static final long DRAWER_ITEM_OFFERS = 1003;
    public static final long DRAWER_ITEM_REGISTER = 1004;
    public static final int DRAWER_ITEM_CALCULATOR = 10005;
    public static final long DRAWER_ITEM_HELP = 1005;
    public static final int DRAWER_ITEM_FAQ = 1006;
    public static final int DRAWER_ITEM_TERMS_AND_CONDITIONS = 100003;

    public static final int DRAWER_ITEM_APP_VERSION = 1110000;
    public static final int DRAWER_ITEM_UPDATE_APP = 1100001;
    public static final long DRAWER_ITEM_LOGOUT = 1272830;
    public static final long DRAWER_ITEM_TRASACTIONS = 23434343;

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
