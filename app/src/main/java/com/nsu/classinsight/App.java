package com.nsu.classinsight;

import android.app.Activity;
import android.app.Application;

import com.nsu.classinsight.di.component.DaggerAppComponent;
import com.nsu.classinsight.utils.AppLogger;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class App extends Application implements HasActivityInjector {

    private static App sInstance;
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    public static App getAppContext() {
        return sInstance;
    }

    private static synchronized void setInstance(App app) {
        sInstance = app;
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);

        AppLogger.init();

    }
}