package com.nsu.classinsight.data.listener;

public interface RecyclerViewOnItemClickListener<T> {
    void onRecyclerViewItemClicked(T object);
}
