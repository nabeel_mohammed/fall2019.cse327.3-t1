package com.nsu.classinsight.data.remote;

public class ApiConstants {
    public static final long CONNECT_TIMEOUT = 30000;
    public static final long READ_TIMEOUT = 30000;
    public static final long WRITE_TIMEOUT = 30000;
    public static final String API_KEY = "UhPPrpEWYiLJoV1E";

    private ApiConstants() {
        // Private constructor to hide the implicit one
    }

}
