package com.nsu.classinsight.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("refresh")
    @Expose
    private String refresh;

    @SerializedName("access")
    @Expose
    private String accessToken;

    @SerializedName("detail")
    @Expose
    private String details;

    public LoginResponse(String refresh, String accessToken) {
        this.refresh = refresh;
        this.accessToken = accessToken;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


}

