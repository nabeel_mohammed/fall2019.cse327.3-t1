
package com.nsu.classinsight.data.local.prefs;


public interface PreferencesHelper {

    String getAccessToken();

    void setAccessToken(String accessToken);

    String getRefreshToken();

    void setRefreshToken(String refreshToken);

    String getPhoneNumber();

    void setPhoneNumber(String phoneNumber);

    boolean getLoggedInStatus();

    void setLoggedInStatus(boolean loggedInStatus);

    void setUserType(String userType);
    String getUserType();

    void setShopSlug(String shopSlug);

    String getShopSlug();


}
