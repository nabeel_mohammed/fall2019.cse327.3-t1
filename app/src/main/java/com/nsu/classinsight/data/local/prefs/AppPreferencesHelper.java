package com.nsu.classinsight.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;


import com.nsu.classinsight.di.PreferenceInfo;

import javax.inject.Inject;


public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_USER_LOGGED_IN_STATUS = "PREF_KEY_USER_LOGGED_IN_STATUS";
    private static final String PREF_KEY_REFRESH_TOKEN = "PREF_KEY_REFRESH_TOKEN";
    private static final String PREF_KEY_PHONE_NUMBER = "PREF_KEY_PHONE_NUMBER";
    private static final String PREF_KEY_USER_TYPE = "PREF_KEY_USER_TYPE";
    private static final String PREF_KEY_SHOP_SLUG = "PREF_KEY_SHOP_SLUG";
    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getRefreshToken() {
        return mPrefs.getString(PREF_KEY_REFRESH_TOKEN, null);
    }

    @Override
    public void setRefreshToken(String refreshToken) {
        mPrefs.edit().putString(PREF_KEY_REFRESH_TOKEN, refreshToken).apply();
    }

    @Override
    public String getPhoneNumber() {
        return mPrefs.getString(PREF_KEY_PHONE_NUMBER, null);
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        mPrefs.edit().putString(PREF_KEY_PHONE_NUMBER, phoneNumber).apply();
    }

    @Override
    public boolean getLoggedInStatus() {
        return mPrefs.getBoolean(PREF_KEY_USER_LOGGED_IN_STATUS, false);
    }

    @Override
    public void setLoggedInStatus(boolean loggedInStatus) {
        mPrefs.edit().putBoolean(PREF_KEY_USER_LOGGED_IN_STATUS, loggedInStatus).apply();
    }

    @Override
    public String getUserType() {
        return mPrefs.getString(PREF_KEY_USER_TYPE, null);
    }

    @Override
    public void setShopSlug(String shopSlug) {
        mPrefs.edit().putString(PREF_KEY_SHOP_SLUG, shopSlug).apply();
    }

    @Override
    public String getShopSlug() {
        return mPrefs.getString(PREF_KEY_SHOP_SLUG, null);
    }

    @Override
    public void setUserType(String userType) {
        mPrefs.edit().putString(PREF_KEY_USER_TYPE, userType).apply();
    }

    public void clearAppPreference() {
        mPrefs.edit().clear().apply();
    }
}
