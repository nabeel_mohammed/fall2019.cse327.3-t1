package com.nsu.classinsight.data.remote;


import androidx.annotation.NonNull;

import com.nsu.classinsight.data.local.prefs.AppPreferencesHelper;
import com.nsu.classinsight.utils.AppConstants;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * This okhttp interceptor is responsible for adding the common query parameters and headers
 * for every service calls
 */
public class RequestInterceptor implements Interceptor {

    private AppPreferencesHelper appPreferencesHelper;

    @Inject
    public RequestInterceptor(AppPreferencesHelper appPreferencesHelper) {
        this.appPreferencesHelper = appPreferencesHelper;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();

        if (appPreferencesHelper.getAccessToken() != null) {
            Request.Builder builder = originalRequest.newBuilder();
            builder.addHeader(AppConstants.AUTHORIZATION, AppConstants.BEARER + appPreferencesHelper.getAccessToken());
            Request request = builder.build();
            return chain.proceed(request);
        }
        return chain.proceed(originalRequest);
    }
}