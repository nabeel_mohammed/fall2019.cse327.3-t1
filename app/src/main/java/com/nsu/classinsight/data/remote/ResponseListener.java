package com.nsu.classinsight.data.remote;

public interface ResponseListener<T, V> {
    void success(T response, int statusCode);
    void error(V error);
}
