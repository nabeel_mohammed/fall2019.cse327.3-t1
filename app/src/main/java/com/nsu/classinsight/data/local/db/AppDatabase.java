
package com.nsu.classinsight.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.nsu.classinsight.data.db.User;
import com.nsu.classinsight.data.local.db.dao.UserDao;


@Database(entities = {User.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}
