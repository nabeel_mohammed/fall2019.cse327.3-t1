package com.nsu.classinsight.data.remote;


import com.nsu.classinsight.data.remote.model.LoginRequest;
import com.nsu.classinsight.data.remote.model.LoginResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @POST("auth/api/login/")
    Call<LoginResponse> loginToServer(@Body LoginRequest user);




}
