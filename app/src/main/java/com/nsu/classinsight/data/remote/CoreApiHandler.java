package com.nsu.classinsight.data.remote;


import android.util.MalformedJsonException;

import com.nsu.classinsight.App;
import com.nsu.classinsight.R;

import java.io.IOException;
import java.net.SocketTimeoutException;

import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * All request will go through it.
 *
 * @param <T>
 */

@Singleton
public class CoreApiHandler<T, V> {

    @SuppressWarnings("unchecked")
    public void createCall(Call<T> call, final ResponseListener<T, V> responseListener) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (responseListener != null) {
                    successProcessor(response, responseListener);
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                responseListener.error((V) getCustomErrorMessage(t));
            }
        });
    }

    /**
     * @param error
     * @return
     */
    private String getCustomErrorMessage(Throwable error) {
        if (error instanceof SocketTimeoutException) {
            return App.getAppContext().getString(R.string.requestTimeOutError);
        } else if (error instanceof MalformedJsonException) {
            return App.getAppContext().getString(R.string.responseMalformedJson);
        } else if (error instanceof IOException) {
            return App.getAppContext().getString(R.string.networkError);
        } else if (error instanceof HttpException) {
            return (((HttpException) error).response().message());
        } else {
            return App.getAppContext().getString(R.string.unknownError);
        }

    }

    /**
     * @param response
     * @return
     */
    @SuppressWarnings("unchecked")
    private void successProcessor(Response<T> response, ResponseListener<T, V> responseListener) {
        if (response.code() == 200 || response.code() == 201) {
            if (response.body() != null) {
                responseListener.success(response.body(), response.code());
            } else {
                serverErrorHandler(response, responseListener);
            }
        }
    }

    private void serverErrorHandler(Response<T> response, ResponseListener<T, V> responseListener) {

    }


}
